package com.example.demo.DAO;

import java.util.ArrayList;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.example.demo.Entity.Employee;

public interface Repository  extends JpaRepository<Employee,Integer>{

	
	@Query(value = "Select id from employee_info e where e.mobile_num=?1",nativeQuery=true)
	public String getEmployeeByMobileNumber(String Mobile_num);
	
	@Query(value="select id from employee_info e where e.email_id=?1",nativeQuery=true)
	public String getEmployeeByEmailID(String Email_id);

	@Query(value="select *from employee_info  ",nativeQuery=true)
	List<Employee> getAllEmployeeData();
	//@Query(value="With CTE AS(Select e.ID, e.NAME, e.AGE FROM employee_info e )Select * From CTE; ",nativeQuery=true)
	//List<Employee> getAllEmployeeinfo();
	
//	@Query(value="With CTE AS(Select e.ID, e.NAME, e.AGE,e.blood_group FROM employee_info e )Select * From CTE; ",nativeQuery=true)
//	List<Object> datap()
	@Query(value="With CTE AS(Select e.ID, e.NAME, e.AGE,e.blood_group FROM employee_info e )Select * From CTE; ",nativeQuery=true)
	List<Object[]> datap();
	@Query(value="With CTE AS(Select e.ID, e.NAME, e.AGE FROM employee_info e )Select * From CTE; ",nativeQuery=true)
	List<Employee> getAllEmployeeinfo();
	
	
}
