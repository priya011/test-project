package com.example.demo.controller;

import java.net.http.HttpHeaders;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import com.example.demo.Entity.Response;


@RestControllerAdvice
public class ValidationHandler  extends ResponseEntityExceptionHandler{
     
	
	  protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex, HttpHeaders headers
			  , HttpStatus status, WebRequest request ){
			Response error =new Response(HttpStatus.BAD_REQUEST,"validationE Error");
			
		  
		  return new ResponseEntity<>(error,HttpStatus.BAD_REQUEST);
		  
}
}