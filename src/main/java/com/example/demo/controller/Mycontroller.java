package com.example.demo.controller;

import java.awt.PageAttributes.MediaType;
import java.net.http.HttpHeaders;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

import javax.validation.Valid;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.BeanWrapper;
import org.springframework.beans.BeanWrapperImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.ModelAndView;

import com.example.demo.DAO.Deptrepository;
import com.example.demo.DAO.Repository;
import com.example.demo.DAO.Workrepo;
import com.example.demo.Entity.Department;
import com.example.demo.Entity.Employee;
import com.example.demo.Entity.Experience;
import com.example.demo.Entity.Response;
import com.example.demo.Entity.Salary;
import com.example.demo.Entity.Work;
import com.example.demo.service.Deptserviceinterface;
import com.example.demo.service.Experienceinterface;
import com.example.demo.service.Serviceinterface;
import com.example.demo.service.saleryinterface;
import com.example.demo.util.pdf.PDFgenerator;

@CrossOrigin(origins = "http://localhost:4200")
@RestController
public class Mycontroller {
@Autowired	
private Serviceinterface ser;
@Autowired
private Deptserviceinterface deptser;
@Autowired
private saleryinterface salin;
@Autowired
private Experienceinterface expin;
@Autowired 
private Deptrepository des;
@Autowired
private Repository repo;
@Autowired
private Workrepo workrepo;
@Autowired
private PDFgenerator pdf;

//Map<String, String> errors;
@GetMapping("/work")
	public String workform(Work wo) {
		return null;
	}


@ResponseStatus(HttpStatus.BAD_REQUEST)
@ExceptionHandler(MethodArgumentNotValidException.class)

public Map<String, String> handleValidationExceptions(MethodArgumentNotValidException ex) {
    Map<String, String> errors = new HashMap<>();
    ex.getBindingResult().getAllErrors().forEach((error) -> {
        String fieldName = ((FieldError) error).getField();
        String errorMessage = error.getDefaultMessage();
        errors.put(fieldName, errorMessage);
    });
    return errors;
}


//getting the list of employee


@RequestMapping(value = "/employees",method = RequestMethod.GET, consumes = "application/json", produces =  "application/json")
public ResponseEntity<List<Employee>> getemployee()
{
	List<Employee> list = new ArrayList<>();
	Iterable<Employee> itemList = this.ser.getemployee();
	for (Employee employee : itemList) {
		list.add(employee);
	}
	
	//System.out.print(list);
	if(list.isEmpty()) {
		return new ResponseEntity<>(null,HttpStatus.INTERNAL_SERVER_ERROR);
	}
	else {
		return new ResponseEntity<>(list,HttpStatus.OK);
	}
	}
//getting the employee from list by id
@GetMapping("/employee/{employeeid}")
public ResponseEntity<Employee> getemployeebyId(@PathVariable Integer employeeid)
{
	Employee emp=new Employee();
	 Optional<Employee> getemployeebyId = this.ser.getemployeebyId(employeeid);
	 //yha kra h present k bad
	 if(getemployeebyId.isPresent() )
	 if(getemployeebyId.get() != null) {
		 return new ResponseEntity<Employee>(getemployeebyId.get(),HttpStatus.OK);
	 }
	 return new ResponseEntity<Employee>(HttpStatus.INTERNAL_SERVER_ERROR);
	 
	
}
//updating the employee information
@PutMapping("/add-employees")
public void getupdatebyId(@RequestBody Employee emp){
	this.ser.getupdatedemployee(emp);
	
	
}
//
@GetMapping("/getdata")
public ResponseEntity<Object> newdata() {
System.out.println("gettting response");
Response resEmp = new Response(HttpStatus.ACCEPTED,"This is done");
	return  ResponseEntity.status(HttpStatus.ACCEPTED).body(resEmp);
	
}







//adding the new employee
 
@PostMapping("/addemployee")          
public ResponseEntity newemployee(@Valid  @RequestBody Employee emp ) {

	//Check all the data
	//Validate all the inputs
	if(checkIfMobileNumberExists(emp) != null) {
		  String empId = checkIfMobileNumberExists(emp);
		  
		  if(!String.valueOf(emp.getId()).equals(empId) || emp.getId() == null) {
			  Response resEmp = new Response(HttpStatus.PRECONDITION_FAILED,"This number is already exist");
				 return ResponseEntity.status(HttpStatus.PRECONDITION_FAILED).body(resEmp);
		  }
	 	}
	  
	  if(checkIfEmailExists(emp) != null) {
		  String empId = checkIfEmailExists(emp);
		  if(!String.valueOf(emp.getId()).equals(empId) || emp.getId() == null) {
			  Response resEmp = new Response(HttpStatus.PRECONDITION_FAILED,"This EmailId is already exist");
				 return ResponseEntity.status(HttpStatus.PRECONDITION_FAILED).body(resEmp);
		  }
	  }
	  
	  		this.ser.newemployee(emp);
			Response resEmp = new Response(HttpStatus.OK,"Saved");
			return ResponseEntity.status(HttpStatus.OK).body(resEmp);
}
@GetMapping("/pdf")	
public void createpdf(PDFgenerator pdf) {
	
	System.out.print(true);
	 this.pdf.generatePdfReport();
	
}	

@GetMapping("/pdf1")	
public void createpdf2(PDFgenerator pdf) {
	
	System.out.print(true);
	Response resEmp=new Response(HttpStatus.ACCEPTED,"pdf is created");
	
	 this.pdf.generatePdfReport1();
	
	
}	
@GetMapping("/getcolumn")
public void getdata() {
	List<Object[]> list=this.repo.datap();
	//System.out.println(list);
	for(Object[] obj :list) {
		//System.out.println(obj.toString());
		System.out.println(Arrays.toString(obj));
	
	}
}
	 
	


private String checkIfEmailExists(Employee emp) {
	 return this.ser.getEmployeeByEmailid(emp.getEmail_id());
	}

private String checkIfMobileNumberExists(Employee emp) {
	return this.ser.getEmployeeByMobileNum(emp.getMobile_num());
	}



//deleting the record in database
 @PostMapping("/deleteemployee/{employeeid}")
 public ResponseEntity<Employee> deletedemployee(@PathVariable ("employeeid") Integer id){
	 this.ser.deleteemployee(id);
	 return new ResponseEntity<>(HttpStatus.OK);
 		
 }
//getting the details of department
@RequestMapping(value= "/dept", method=RequestMethod.GET, consumes= "application/json", produces="application/json")
 public ResponseEntity<List<Department>> getdepartment()
 {
	List<Department> list=new ArrayList<>();
	Iterable<Department> itemlist=this.deptser.getdept();
	for(Department department:itemlist) {
		list.add(department);
	}
	if(list.isEmpty()) {
		return new ResponseEntity<>(null,HttpStatus.INTERNAL_SERVER_ERROR);
	}else {
	 return new ResponseEntity<>(list,HttpStatus.OK);
 }}
//adding the new department
//@PostMapping("/adddept")
/*
public ResponseEntity <Object> newdepartmnet(@RequestBody Department dep,BindingResult bindingresult)
{   
	if(bindingresult.hasErrors()) {
		empo=new HashMap<>();
	for(FieldError error: bindingresult.getFieldErrors()) {
		empo.put(error.getField(), error.getDefaultMessage());
	}	
	return new ResponseEntity<>(emp0,HttpStatus.NOT_ACCEPTABLE);	
	}
	Optional<Department> depart= des.findById(dep.getDept_id());
	if(depart!=null) {
	
		return new ResponseEntity<>(HttpStatus.CONFLICT);
	}
	return new ResponseEntity<>(des.save(dep),HttpStatus.OK); 
}
/*
@GetMapping("/deptname/{deptid}")
public ResponseEntity<Department> findbydeptid(@PathVariable  Integer deptid){
	
	Optional<Department> findbydeptid=this.deptser.findbydeptid(deptid);
	if( findbydeptid.isPresent())
		if(findbydeptid.get()!=null) {
			return new ResponseEntity<Department>(findbydeptid.get(),HttpStatus.OK);
		 }
		 return new ResponseEntity<Department>(HttpStatus.INTERNAL_SERVER_ERROR);
		
	
	
}
*/
//deleting the details of department
@PostMapping("/deletedept/{deptid}")
public ResponseEntity<Department> deletedepartment(@PathVariable("deptid") Integer id)
{
this.deptser.deletedepartment(id);	
 return new ResponseEntity<>(HttpStatus.OK);
}
//getting the details of salary
@GetMapping("/salary")
public List<Salary> getsalary()
{
	return this.salin.getsalary();
	
}

//adding the details of salary
@PostMapping("/addsalary")
public Salary newsalary(@RequestBody Salary sal ) 
{
	return this.salin.newsalary(sal);
	
}
//deleting the salary_details	
@DeleteMapping("/salary/{salaryid}")
public void deletesalary(@PathVariable("salaryid") Integer sal_id)
{
	 this.salin.deletedsalary(sal_id);
}

//updating the details
@PutMapping("/salary")
public void updatesalary(@RequestBody Salary sal)
{
	salin.updatedsalary(sal);
	
}
 //
@PostMapping("/addExpdata")
public Experience newdata(@RequestBody Experience exp ) 
{
	return this.expin.newdata(exp);
	
}
 
@RequestMapping(value="experience", method = RequestMethod.GET,consumes ="application/json", produces="application/json")
public ResponseEntity<List<Experience>> getExpData()
{
	List<Experience> list =new ArrayList<>();
	Iterable<Experience> itemList = this.expin.getexpdata();
	for (Experience exp : itemList) {
		list.add(exp);
	}
	System.out.print(list);
		if(list.isEmpty()) {
		return new ResponseEntity<>(null,HttpStatus.INTERNAL_SERVER_ERROR);
	}
	else {
		return new ResponseEntity<>(list,HttpStatus.OK);
	}
}
@PostMapping("/deleteexp/{expid}")
public ResponseEntity<Experience> deleteexperience(@PathVariable("expid") Integer id ){
	 this.expin.deleteexperience(id);
	return new ResponseEntity<>(HttpStatus.OK) ;
	
}

@GetMapping("/Exp/{expid}")
public ResponseEntity<Experience> findbyExpid(@PathVariable  Integer expid){
	
	Optional<Experience> findbyExpid=this.expin.findbyExpid(expid);
	if( findbyExpid.isPresent())
		if(findbyExpid.get()!=null) {
			return new ResponseEntity<Experience>(findbyExpid.get(),HttpStatus.OK);
		 }
		 return new ResponseEntity<Experience>(HttpStatus.INTERNAL_SERVER_ERROR);
		
	
	
}

}

