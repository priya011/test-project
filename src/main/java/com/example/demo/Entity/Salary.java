package com.example.demo.Entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="salary_info")
public class Salary {
@Id
@GeneratedValue(strategy=GenerationType.IDENTITY)
private int sal_id;
private int id;
private int dept_id;
private int salary;
public Salary() {
	
}
public Salary(int sal_id, int id, int dept_id, int salary) {
	super();
	this.sal_id = sal_id;
	this.id = id;
	this.dept_id = dept_id;
	this.salary = salary;
}
public int getSal_id() {
	return sal_id;
}
public void setSal_id(int sal_id) {
	this.sal_id = sal_id;
}
public int getId() {
	return id;
}
public void setId(int id) {
	this.id = id;
}
public int getDept_id() {
	return dept_id;
}
public void setDept_id(int dept_id) {
	this.dept_id = dept_id;
}
public int getSalary() {
	return salary;
}
public void setSalary(int salary) {
	this.salary = salary;
}
@Override
public String toString() {
	return "Salary [sal_id=" + sal_id + ", id=" + id + ", dept_id=" + dept_id + ", salary=" + salary + "]";
}



}
