package com.example.demo.Entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;


@Entity
public class Work {
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY )
	@Column(updatable = false, nullable = false)
    private Integer workid;
	private String name;
	private String pancard;
	private String adharcard;
	public Work() {
		
	}
	public Work( Integer workid,String name, String pancard, String adharcard) {
		 this.workid=workid;
		this.name = name;
		this.pancard = pancard;
		this.adharcard = adharcard;
	}
	public Integer getworkid() {
		return workid;
	}
	public void setworkid(Integer workid) {
		this.workid= workid;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getPancard() {
		return pancard;
	}
	public void setPancard(String pancard) {
		this.pancard = pancard;
	}
	public String getAdharcard() {
		return adharcard;
	}
	public void setAdharcard(String adharcard) {
		this.adharcard = adharcard;
	}
	@Override
	public String toString() {
		return "Work [workid="+workid+",name=" + name + ", pancard=" + pancard + ", adharcard=" + adharcard + "]";
	}
	
}
