package com.example.demo.Entity;

import java.util.List;

import org.springframework.http.HttpStatus;

public class Response {
    
	private HttpStatus status;
	private String message;
	
	//private List<Department> data1;
	
	
	
	public Response(HttpStatus httpstatus, String message ) {
		this.status = httpstatus;
		this.message = message;
		
		
	}

	public String getResponseMessage() {
		return this.message;
		
	}
	
	
}
