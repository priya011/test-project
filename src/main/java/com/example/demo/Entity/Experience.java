package com.example.demo.Entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import java.sql.Date;

@Entity
@Table(name="experience_infoo")
public class Experience 
{
@Id
@GeneratedValue(strategy=GenerationType.IDENTITY)
@Column(updatable = false, nullable = false)
private int exp_id;

private int emp_id;
private Date date_of_joining;
private Date date_of_release;
private int numb_of_experience;
public Experience() {
	
}
public Experience(int exp_id, int emp_id, Date date_of_joining, Date date_of_release, int numb_of_experience) {
	super();
	this.exp_id = exp_id;
	this.emp_id = emp_id;
	this.date_of_joining = date_of_joining;
	this.date_of_release = date_of_release;
	this.numb_of_experience = numb_of_experience;
}
public int getExp_id() {
	return exp_id;
}
public void setExp_id(int exp_id) {
	this.exp_id = exp_id;
}
public int getEmp_id() {
	return emp_id;
}
public void setEmp_id(int emp_id) {
	this.emp_id = emp_id;
}
public Date getDate_of_joining() {
	return date_of_joining;
}
public void setDate_of_joining(Date date_of_joining) {
	this.date_of_joining = date_of_joining;
}
public Date getDate_of_release() {
	return date_of_release;
}
public void setDate_of_release(Date date_of_release) {
	this.date_of_release = date_of_release;
}
public int getNumb_of_experience() {
	return numb_of_experience;
}
public void setNumb_of_experience(int numb_of_experience) {
	this.numb_of_experience = numb_of_experience;
}
@Override
public String toString() {
	return "Experience [exp_id=" + exp_id + ", emp_id=" + emp_id + ", date_of_joining=" + date_of_joining
			+ ", date_of_release=" + date_of_release + ", numb_of_experience=" + numb_of_experience + "]";
}


	
}
