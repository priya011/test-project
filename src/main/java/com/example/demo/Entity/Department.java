package com.example.demo.Entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;

@Entity
@Table(name="department_info")
public class Department {

@Id
@GeneratedValue(strategy=GenerationType.IDENTITY)
@Column(updatable = false, nullable = false)
private int dept_id;
@NotEmpty(message="name cannot be empty")
private String dept_name;
private int id;
public int getDept_id() {
	return dept_id;
}
public void setDept_id(int dept_id) {
	this.dept_id = dept_id;
}
public String getDept_name() {
	return dept_name;
}
public void setDept_name(String dept_name) {
	this.dept_name = dept_name;
}
public int getId() {
	return id;
}
public void setId(int id) {
	this.id = id;
}
public Department(int dept_id, String dept_name, int id) {
	
	this.dept_id = dept_id;
	this.dept_name = dept_name;
	this.id = id;
}
public Department() {
	
}
@Override
public String toString() {
	return "Department [dept_id=" + dept_id + ", dept_name=" + dept_name + ", id=" + id + "]";
}

}
