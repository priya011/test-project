package com.example.demo.service;



import java.util.List;
import java.util.Optional;


import com.example.demo.Entity.Employee;
import com.example.demo.Entity.Experience;


public interface Serviceinterface {

	public List<Employee> getemployee();

	public Optional<Employee> getemployeebyId(Integer employeeid);

	Employee newemployee(Employee emp);

	public void getupdatedemployee(Employee emp);

	public void deleteemployee(Integer id);
	
	public String getEmployeeByMobileNum(String mobile_num);
   
	public String getEmployeeByEmailid(String email_id );
	//public void getEmployeebyId(Integer employeeid);
	

	

	

	

	

	


	
}
