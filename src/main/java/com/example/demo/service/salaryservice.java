package com.example.demo.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PathVariable;
import com.example.demo.DAO.salaryrepository;

import com.example.demo.Entity.Salary;

@Service
public  class salaryservice implements saleryinterface {
@Autowired
	salaryrepository salrepo;

@Override
public List<Salary> getsalary() {
	
	return salrepo.findAll();
}

@Override
public Salary newsalary(Salary sal) {
	
	return salrepo.save(sal);
}

@Override
public void deletedsalary(Integer sal_id) {
	 salrepo.deleteById(sal_id);
}

@Override
public void updatedsalary(Salary sal) {
	salrepo.save(sal);
	
}



}
