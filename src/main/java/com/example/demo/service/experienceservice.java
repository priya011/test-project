package com.example.demo.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PathVariable;

import com.example.demo.DAO.ExperienceDAO;
import com.example.demo.Entity.Experience;

@Service
public class experienceservice implements Experienceinterface{
	@Autowired
    private ExperienceDAO expdao;

	@Override
	public Experience newdata(Experience exp) {
		// TODO Auto-generated method stub
		return expdao.save(exp);
	}

	@Override
	public List<Experience> getexpdata() {
		
		return expdao.findAll();
	}

	@Override
	public String mostexp() {
		
		return expdao.getmostexp();
	}

	@Override
	public void deleteexperience(Integer id) {
		 expdao.deleteById(id);
		
	}

	@Override
	public Optional<Experience> findbyExpid(@PathVariable(value="expid")Integer expid) {
		// TODO Auto-generated method stub
		return expdao.findById(expid);
	}


		

}
