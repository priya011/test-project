package com.example.demo.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PathVariable;

import com.example.demo.DAO.Deptrepository;

import com.example.demo.Entity.Department;
@Service
public class Deptservice implements Deptserviceinterface {
@Autowired
	 private Deptrepository repo;
	
	@Override
	public List<Department> getdept() {
		 
		return repo.findAll();
	}

	@Override
	public Department adddept( Department dep) {
		return repo.save(dep);
	}

	@Override
	public void deletedepartment(Integer dept_id) {
		repo.deleteById(dept_id);
		
	}

	@Override
	public void findbydeptid(Integer deptid) {
		// TODO Auto-generated method stub
		repo.findById(deptid);
	}


	

	
   
	
	
}
