package com.example.demo.service;

import java.util.List;
import java.util.Optional;
import java.util.function.Supplier;

import javax.persistence.OneToMany;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;

import com.example.demo.DAO.Repository;
import com.example.demo.Entity.Employee;
@Service
public class Serviceclass implements Serviceinterface {
private static final Object Employee = null;
@Autowired
	Repository repo;
	
//method for getting all the list of employees
	@Override
	public List<Employee> getemployee()
	{
		
		return  repo.findAll();
	}	


//method for getting the employee by Id
	@Override
	public Optional<Employee> getemployeebyId(@PathVariable(value="employeeid")Integer id) {
		
		return repo.findById(id);
	}
	
//method for adding the new employee
	@Override
	public Employee newemployee(Employee emp) {
		// 
		return repo.save(emp);
	}

	//method for deleting the employee

	@Override
	public void deleteemployee(Integer id) {
		repo.deleteById(id);
	}

	
//method for updating the employee
	@Override
	public void getupdatedemployee(Employee emp) {
		// TODO Auto-generated method stub
		
		Optional<com.example.demo.Entity.Employee> empExist = repo.findById(emp.getId());
		if(empExist.isPresent()) {
			empExist.get().setAge(emp.getAge());
			empExist.get().setBlood_group(emp.getBlood_group());
//			empExist.get().setEmail_id(emp.getEmail_id());
//			empExist.get().setMobile_num(emp.getMobile_num());
			empExist.get().setName(emp.getName());
			repo.save(empExist.get());
			
		}
		

	}


	@Override
	public String getEmployeeByMobileNum(String mobile_num) {
		return repo.getEmployeeByMobileNumber(mobile_num);
		
	}


	@Override
	public String getEmployeeByEmailid(String email_id) {
		// TODO Auto-generated method stub
		return repo.getEmployeeByEmailID(email_id);
	}


	
	


	



}